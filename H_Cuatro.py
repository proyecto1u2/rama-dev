#!/usr/bin/python3
# -*- coding: utf-8 -*-


# Que pais tiene mas desnutricion y cual tiene menos (...)  (?)


# Llamar a la(s) librería(s) necesaria(s)(Matpotlib, Pandas y Opener {Función
# propia}) y extraer funcionalidades a utilizarse en el desarrollo del
# programa.

import pandas as pd
import matplotlib.pyplot as plt
import opener


# Se construyen las funciones módulo por módulo y luego se añaden a una última
# función main:

#   1er Módulo: Función de creación variable desnutricion
def data_desnutridos(data):
    desnutridos = data["Undernourished"].copy()
    desnutridos = data[(data["Undernourished"] != "NA") &
                       (data["Undernourished"].notna())].copy()
    return desnutridos


#   2do Módulo: Función de creación mínimos y máximos en desnutricion
def min_max(desnutridos):
    minimus = desnutridos[desnutridos.Undernourished ==
                          desnutridos.Undernourished.min()]
    maximus = desnutridos[desnutridos.Undernourished ==
                          desnutridos.Undernourished.max()]
    return minimus, maximus


#   3er Módulo: Creación gráficos desnutrición
def grafico_desnutridos(data, desnutridos):
    desnutridos.plot(kind="scatter", x="Country", y="Undernourished")
    plt.savefig("Desnutridos.png")


#   4to Módulo: Función principal
def main():
    new_data = opener.obtener_columnas(["Country", "Undernourished"])
    new_data.loc[new_data['Undernourished']
                 == '<2.5', 'Undernourished'] = '2.0'
    new_data['Undernourished'] = pd.to_numeric(new_data['Undernourished'])
    desnutridos = data_desnutridos(new_data)
    minimus, maximus = min_max(desnutridos)

    print(desnutridos)
    print(f"\n\n{minimus} \n\n{maximus}\n")
    grafico_desnutridos(new_data, desnutridos)
    return desnutridos


main()
