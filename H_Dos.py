#!/usr/bin/python3
# -*- coding: utf-8 -*-


# Llamar a la(s) librería(s) necesaria(s)(Matpotlib, Pandas y Opener {Función
# propia}) y extraer funcionalidades a utilizarse en el desarrollo del
# programa.

import matplotlib.pyplot as plt
import opener
import pandas as pd


# Se construyen las funciones módulo por módulo y luego se añaden a una última
# función main:

#   1er Módulo: función de creación variable foodob
def data_ob(data):
    foodob = data["Obesity"].copy()
    foodob = data[(data["Obesity"].notna())].copy()
    foodob["Obesity"] = foodob["Obesity"] / 100
    return foodob


#   2do Módulo: Función de creación variable finados
def death_var(data):
    finados = data["Deaths"].copy()
    finados = data[(data["Deaths"].notna())].copy()
    finados["Deaths"] = finados["Deaths"] / 100
    return finados


#   3er Módulo: Creación gráficos obesidad
def graphic_dos_obesity(dataob):
    dataob.plot(kind="bar", x="Country", y="Valor Real Obesidad",
                subplots=True, figsize=(5, 7))
    plt.savefig("graphic_dos_obesity.png")


#   4to Módulo: Creación gráficos Deaths
def graphic_dos_muertes(datafnds):
    datafnds.plot(kind="bar", x="Country", y="Valor Real Muertos",
                  subplots=True, figsize=(5, 7))
    plt.savefig("graphic_dos.png")


#   5er Módulo: Función principal
def main():
    # Se crea fórmula para representar las columnas "Country - Obesity
    # - Population"
    new_dataob = opener.obtener_columnas(["Obesity", "Population", "Country"])
    dataob = data_ob(new_dataob)
    dataob["Valor Real Obesidad"] = dataob["Obesity"] * dataob["Population"]
    dataob.sort_values(by="Valor Real Obesidad", inplace=True, ascending=False)
    dataobb = dataob.head(5)

    # Formulación de gráfico de la primera tabla
    graphic_dos_obesity(dataobb)

    # Se crea fórmula para representar las columnas "Country - Deaths
    # - Population"
    new_dataf = opener.obtener_columnas(["Population", "Deaths", "Country"])
    dataf = death_var(new_dataf)
    dataf["Valor Real Muertos"] = dataf["Deaths"] * dataf["Population"]
    dataf.sort_values(by="Valor Real Muertos", inplace=True, ascending=False)
    datafnds = dataf.head(5)

    # Formulación de grafico de segunda tabla
    graphic_dos_muertes(datafnds)

    # Se imprimen datos
    print(f"\n{dataobb}")

    print(f"\n{datafnds}")

    return datafnds
