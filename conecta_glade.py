#!/usr/bin/python3
# -*- coding: utf-8 -*-


# INTERFAZ GRÁFICA DEL PROYECTO DE PROGRAMACIÓN


# Se llaman e importan las librerías necesarias para la construcción
# del código

import aditional
import pandas
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import H_Uno
import H_Dos
import H_Tres
import H_Cuatro
import H_Cinco


# Clases y funciones
class MainWindows():
    # Se define función de constructor
    def __init__(self):
        # Variable constructor
        self.builder = Gtk.Builder()
        # Se añade el archivo .ui desde la carpeta que lo aloja
        self.builder.add_from_file("./UI2.glade")

        self.window = self.builder.get_object("main_windows")
        # La ventana se designa como
        self.window.set_title("Análisis de dataset del USDA")
        # Al pulsar en el botón 'close' cierra completamente la actividad
        self.window.connect("destroy", Gtk.main_quit)
        # Se redimensiona el tamaño de la ventana
        self.window.set_default_size(800, 600)
        # Mostrar la ventana gráfica
        self.window.show_all()

        # Creación del combobox
        self.combobox = self.builder.get_object("combobox_hipotesis")
        self.hipotesis = Gtk.ListStore(str)
        # Rectángulo que puede contener texto
        cell = Gtk.CellRendererText()
        # Permite crear parámetros
        self.combobox.pack_start(cell, True)
        # Creación de opciones y parámetros del combobox
        self.combobox.add_attribute(cell, "text", 0)
        preguntas = ["Pregunta 1",
                     "Pregunta 2",
                     "Pregunta 3",
                     "Pregunta 4",
                     "Pregunta 5"]

        # Recorredor de las preguntas
        for pregunta in preguntas:
            self.hipotesis.append([pregunta])

        # Menú
        self.combobox.set_model(model=self.hipotesis)
        self.combobox.connect("changed", self.seleccion)

        # Label donde se presentan las preguntas en la interfaz gráfica
        self.nombre_H = self.builder.get_object("hipotesis_x")
        self.grafico = self.builder.get_object("grafico")

        self.treeview = self.builder.get_object("datos_treeview")
        self.treeview.connect("cursor-changed", self.load_aditional)

        # Información adicional a la pregunta presentada
        self.info_adicional = self.builder.get_object("contexto_hipotesis")

    # Iterador de las opciones-preguntas presentadas en el combobox
    def seleccion(self, btn=None):
        indice = self.combobox.get_active_iter()
        modelo = self.combobox.get_model()
        opcion = modelo[indice][0]
        self.nombre_H.set_text(f"escogi {opcion}")

        # Opción que presenta la hipótesis asociada a la pregunta 1
        if opcion == "Pregunta 1":
            self.nombre_H.set_text(f"¿Cuáles son los 5 paı́ses que tienen \n"
                                   "más habitantes con obesidad? Esta\n"
                                   "pregunta no es una cifra porcentual,sino\n"
                                   "que real, por lo que se debe realizar el\n"
                                   "cálculo a partir de la columna población\n"
                                   "(Population)\n"
                                   )

            # Muestra gráfico de la H_1 en la interfaz
            self.grafico.set_from_file("./graphic_uno.png")
            dataframe = H_Uno.main()

        # Opción que presenta la hipótesis asociada a la pregunta 2
        elif opcion == "Pregunta 2":
            self.nombre_H.set_text(f"¿Cuáles son los 5 paı́ses que tienen\n"
                                   "más habitantes con obesidad y que además\n"
                                   "los que más muertes tienen? Esta\n"
                                   "respuesta es distinta a la anterior,\n"
                                   "pues no necesariamente un paı́s que esté\n"
                                   "en el top5 de obesidad también tenga una\n"
                                   "mayor cantidad de muertes en cifras. Las\n"
                                   "cifras son en tipo entero, no porcentual\n"
                                   )

            # Muestra gráfico de la H-2 en la interfaz
            self.grafico.set_from_file("./graphic_dos.png")
            self.grafico.set_from_file("./graphic_dos_obesity.png")
            dataframe = H_Dos.main()

        # Opción que presenta la hipótesis asociada a la pregunta 3
        elif opcion == "Pregunta 3":
            self.nombre_H.set_text(f"¿Los paı́ses que tienen mayor consumo\n"
                                   "de alcohol son los paı́ses que más\n"
                                   "contagios confirmados tienen? graficar\n"
                                   "el top10 de paı́ses más consumo de\n"
                                   "alcohol tienen vs más contagios activos\n"
                                   "tienen (scatter plot)\n"
                                   )

            # Muestra gráfico de la H-3 en la interfaz
            self.grafico.set_from_file("./grafico_Tres.png")
            dataframe = H_Tres.main()

        # Opción que presenta la hipótesis asociada a la pregunta 4
        elif opcion == "Pregunta 4":
            self.nombre_H.set_text(f"¿Qué país tiene el porcentaje más alto\n"
                                   "de desnutrición y cual tiene menos\n"
                                   )

            # Muestra gráfico de la H-4 en la interfaz
            self.grafico.set_from_file("./Desnutridos.png")
            dataframe = H_Cuatro.main()

        # Opción que presenta la hipótesis asociada a la pregunta 5
        elif opcion == "Pregunta 5":
            self.nombre_H.set_text(f"Comparar datos de COVID para paises con\n"
                                   "mayor probabilidad de estabilidad\n"
                                   "economica ante COVID de America del Sur\n"
                                   )

            # Muestra gráfico de la H-5 en la interfaz
            self.grafico.set_from_file("./grafico_cinco.png")
            dataframe = H_Cinco.main()

        # Presenta las tablas asociadas a las hipótesis correspondientes
        if len(self.treeview.get_columns()) != 0:
            for col in self.treeview.get_columns():
                self.treeview.remove_column(col)

        # Conversión de tipo de las tablas
        n_columnas = len(dataframe.columns)
        self.lista_valores = Gtk.ListStore(*(n_columnas * [str]))
        cell = Gtk.CellRendererText()

        for columna in range(n_columnas):
            col = Gtk.TreeViewColumn(dataframe.columns[columna], cell, text=columna)
            self.treeview.append_column(col)

        self.treeview.set_model(model=self.lista_valores)

        for value in dataframe.values:
            fila = [str(linea) for linea in value]
            self.lista_valores.append(fila)

    # Modúlo que presentará la información adicional a la pregunta
    def load_aditional(self, btn=None):
        model, index = self.treeview.get_selection().get_selected()

        if model is None or index is None:
            return

        # Data asociada a cada país de manera independiente
        pais = model.get_value(index, 0)
        aditional_data = aditional.get_aditional_data(pais)
        texto = "Datos adicionales sobre el pais: \n"

        for data in aditional_data:
            texto += data + ": " + str(aditional_data[data]) + "\n"

        self.info_adicional.set_text(texto)


# Llamado a ventana principal y ejecución
if __name__ == "__main__":
    MainWindows()
    Gtk.main()
