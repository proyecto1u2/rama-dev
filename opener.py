#funcion que abre las columnas de pandas
import pandas as pd

def obtener_columnas(columnas):
    COLUMNAS = ["Country",
             "Alcoholic Beverages",
             "Animal Products",
             "Animal fats",
             "Aquatic Products, Other",
             "Cereals - Excluding Beer",
             "Eggs",
             "Fish, Seafood",
             "Fruits - Excluding Wine",
             "Meat",
             "Milk - Excluding Butter",
             "Miscellaneous",
             "Offals",
             "Oilcrops",
             "Pulses",
             "Spices",
             "Starchy Roots",
             "Stimulants",
             "Sugar Crops",
             "Sugar & Sweeteners",
             "Treenuts",
             "Vegetal Products",
             "Vegetable Oils",
             "Vegetables",
             "Obesity",
             "Undernourished",
             "Confirmed",
             "Deaths",
             "Recovered",
             "Active",
             "Population",
             "Unit (all except Population)"]
             
    data = pd.read_csv("suministro_alimentos_kcal.csv")

    for i in columnas:
        COLUMNAS.remove(i)

    data.drop(COLUMNAS, axis=1, inplace=True)

    return data

