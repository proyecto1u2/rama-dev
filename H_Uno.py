#!/usr/bin/python3
# -*- coding: utf-8 -*-


# Llamar a la(s) librería(s) necesaria(s)(Matpotlib, Pandas y Opener {Función
# propia}) y extraer funcionalidades a utilizarse en el desarrollo del
# programa.

import matplotlib.pyplot as plt
import opener
import pandas as pd


# Se construyen las funciones módulo por módulo y luego se añaden a una última
# función main:

#   1er Módulo: función de creación variable foodob
def data_ob(data):
    foodob = data["Obesity"].copy()
    foodob = data[(data["Obesity"].notna())].copy()
    foodob["Obesity"] = foodob["Obesity"] / 100
    return foodob


#   2do Módulo: Creación gráficos
def graphic_uno(dataob):
    dataob.plot(kind="bar", x="Country", subplots=True, figsize=(6, 8))
    plt.savefig("graphic_uno.png")


#   3er Módulo: Función principal
def main():
    new_data = opener.obtener_columnas(["Obesity", "Population", "Country"])
    dataob = data_ob(new_data)
    dataob["Valor Real Obesidad"] = dataob["Obesity"] * dataob["Population"]
    dataob.sort_values(by="Valor Real Obesidad", inplace=True, ascending=False)
    dataobb = dataob.head(5)

    graphic_uno(dataobb)

    print(f"\n{dataobb}")
    return dataobb
