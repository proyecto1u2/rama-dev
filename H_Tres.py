#!/usr/bin/python3
# -*- coding: utf-8 -*-


# Llamar a la(s) librería(s) necesaria(s)(Matpotlib, Pandas y Opener {Función
# propia}) y extraer funcionalidades a utilizarse en el desarrollo del
# programa.

import matplotlib.pyplot as plt
import opener
import pandas as pd


# Se construyen las funciones módulo por módulo y luego se añaden a una última
# función main:

#   1er Módulo: Función de creación Top10 de países líderes en el consumo de
#               bebidas alcohólicas
def data_alcohol(data):
    alcohol = data["Alcoholic Beverages"].copy()
    alcohol = data[(data["Alcoholic Beverages"] > 3.0615)].copy()
    alcohol.drop(["Confirmed"], axis=1, inplace=True)

    return alcohol


#   2do Módulo: Función de creación Top10 de países con más contagios
def data_contagios(data):
    contagios = data["Confirmed"].copy()
    contagios = data[(data["Confirmed"] > 2.7)].copy()
    contagios.drop(["Alcoholic Beverages"], axis=1, inplace=True)

    return contagios


#   3er Módulo: Creación de gráficos comunes a ambos módulos anteriores
def data_ambas(data):
    ambas = data[(data["Confirmed"] > 2.7) |
                 (data["Alcoholic Beverages"] > 3.0615)].copy()

    return ambas


# Gráfico top10 países consumidores de bebidas alcohólicas y países con más
# contagios
def grafico_contagiados(ambas):
    ax = ambas.plot(kind="scatter", x="Country",
                    y="Alcoholic Beverages", color="Red", label="Data Alcohol")
    ambas.plot(kind="scatter", x="Country", y="Confirmed",
               color="Blue", label="Data Contagios", ax=ax)
    plt.savefig("grafico_Tres.png")


# Main
def main():
    new_data = opener.obtener_columnas(
        ["Country", "Alcoholic Beverages", "Confirmed"])
    ambas = data_ambas(new_data)
    # alcohol = data_alcohol(new_data)
    # contagios = data_contagios(new_data)

    print(f"{ambas}\n")
    grafico_contagiados(ambas)
    return ambas


main()
