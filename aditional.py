#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Llamar a la(s) librería(s) necesaria(s) (Pandas) y extraer funcionalidades a # utilizarse en el desarrollo del programa.

import pandas as pd


# 1er Módulo: Genera la información adicional de cada país de manera dinámica
def get_aditional_data(country):

    data = pd.read_csv("./suministro_alimentos_kcal.csv")
    aditional_data = {}

    # Data que será presentada como info adicional
    country_data = data[["Country", "Population", "Confirmed", "Active",
                         "Deaths"]].copy()

    country_data = country_data[(country_data["Country"] == country)]

    dataname = country_data.columns
    for value in country_data.values:
        for i in range(len(value)):
            aditional_data[dataname[i]] = value[i]

    return aditional_data
