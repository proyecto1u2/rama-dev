# Rama-dev

Rama donde se alojarán los progresos y avances del proyecto



# Análisis de dataset del USDA
El siguiente proyecto consiste en un programa que va orientado al análisis de datos y programación del mismo.
Se nos presenta un archivo de texto o Dataset referente a "Centro de Política y Promoción de la Nutrición del USDA"
Donde nuestro principal objetivo es responder una serie de preguntas, con datos obtenidos del Dataset.
Además, este programa estará dotada de una UI (Interfaz Gráfica), donde el usuario podrá elegir a partir de un menú una serie de opciones, las cuales revelarán su hipótesis, gráfico y conclusión correspondiente

# Historia:
Fue escrito y desarrollado en conjunto por los autores Jorge Carrillo Silva, Benjamín Vera Garrido y Michelle Valdés Méndez, utilizando para ello el lenguaje de programación Python, específicamente la versión 3.8.5
El código del programa se rige bajo las indicaciones de la PEP-8 (Style Guide for Python Code), que busca generar un código limpio y óptimo, además de una correcta indentación y sintaxis.
Contiene 3 librerías, las cuales son:
Matplot.lib (Para la generación de gráficos de distintos estilos), Pandas.lib (Para el análisis de datos y su manejo) y gi (Librería que genera la interfaz gráfica, al importarse Gtk 3.0)

# Para empezar:
Es requisito tener instalado Python 3.6+ (de preferencia la versión 3.8.5) y está pensada que su ejecución sea si o sí en un entorno Linux, tales como Debian y sus derivados, etc.
También es necesario tener instaladas las librería Matplot.lib, Pandas.lib, gi y PyGObject (Paquete de Python que proporciona enlaces para bibliotecas basadas en GObject, tales como GTK, GStreamer, WebKitGTK, Glib, GIO y más)

# Instalación:
Para instalar y ejecutar el programa en su máquina, es necesario que siga las presentes indicaciones:
1- Clonar el repositorio "Análisis de dataset del USDA" correspondiente a la versión final a presentarse del código, en el directorio de su preferencia, el enlace HTTPS es https://gitlab.com/proyecto1u2/rama-stable.git .
2- Luego, deberá entrar en la carpeta clonada que lleva por nombre "P1-U2" que contiene 13 archivos, es esencial que pueda comprobar y cerciorarse que se encuentran los paquetes y librerías esenciales, ya que de lo contrario el programa podría no funcionar adecuadamente (conecta_glade.py, H-{1, 2, 3, 4 y 5}, UI2.glade, suministro_alimentos_kcal.csv, proyecto1.pdf y README.md)
3- Posteriormente, ya está usted preparado para ejecutar el programa. Para ello, abrirá una terminal y en el directorio que tenga los archivos, ejecutará el siguiente comando.
python3 conecta_glade.py

# Codificación:
El programa se construyó pensando en el soporte de la codificación UTF-8.

# Construido con:
Atom: Uno de los 3 IDE's utilizados para desarrollar el proyecto.
Visual Studio Code: Gracias a su facilidad de uso y terminal integrada, fue utilizada para construir el proyecto
VSCodium: Junto con Atom y Visual, es uno de los Entornos de desarrollo utilizados para el desarrollo del proyecto.

# Licencia:
Este proyecto está sujeto bajo la Licencia GNU GPL v3.

# Autores y creadores del proyecto:
Jorge Carrillo Silva, Benjamín Vera Garrido y Michelle Valdés Méndez.

# Agradecimientos:
Queremos agradecer a todos aquellos que nos han ayudado y nos han orientado en el desarrollo del proyecto, en especial al profesor Fabio Durán y al ayudante Arturo Lobos.
