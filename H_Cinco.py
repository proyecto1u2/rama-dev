#!/usr/bin/python3
# -*- coding: utf-8 -*-


# Llamar a la(s) librería(s) necesaria(s)(Matpotlib, Pandas y Opener {Función
# propia}) y extraer funcionalidades a utilizarse en el desarrollo del
# programa.


import matplotlib.pyplot as plt
import opener
import pandas as pd


# Se construyen las funciones módulo por módulo y luego se añaden a una última
# función main:

#   1er Módulo: función "abrir archivo" y "leer archivo"
def datas_paises():
    data = pd.read_csv("./suministro_alimentos_kcal.csv",
                       index_col="Country")
    paises_data = data.loc[["Colombia",
                            "Chile",
                            "Peru",
                            "Paraguay",
                            "Uruguay"]]

    return paises_data


#   2do Módulo: Eliminación de datos innecesarios para la evaluación de datos
#               requeridos
def filtrado(paises_data):
    paises_data.drop(['Population',
                      'Unit (all except Population)',
                      'Meat',
                      'Eggs',
                      'Undernourished',
                      'Animal Products',
                      'Alcoholic Beverages',
                      'Animal fats',
                      'Aquatic Products, Other',
                      'Cereals - Excluding Beer',
                      'Fish, Seafood',
                      'Fruits - Excluding Wine',
                      'Milk - Excluding Butter',
                      'Miscellaneous',
                      'Offals',
                      'Oilcrops',
                      'Pulses',
                      'Spices',
                      'Starchy Roots',
                      'Stimulants',
                      'Sugar Crops',
                      'Sugar & Sweeteners',
                      'Treenuts',
                      'Vegetal Products',
                      'Vegetable Oils',
                      'Vegetables',
                      'Obesity'
                      ],
                     axis=1,
                     inplace=True)

    return paises_data


#   3er Módulo: Creación gráficos de países
def grafico_paises(paises_data):
    fig = paises_data.plot(kind="bar")
    fig.figure.savefig("grafico_cinco.png")


#   4to Módulo: Función principal
def main():
    paises_data = datas_paises()
    paises_data = filtrado(paises_data)

    # print(f"\n{paises_data}\n")
    # print(paises_data.columns)

    grafico_paises(paises_data)

    return paises_data


main()
